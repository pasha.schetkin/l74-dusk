<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\ArticlesController::class, 'index'])->name('dashboard');

Route::prefix('admin')->middleware(['auth', 'admin'])->name('admin.')->group(function () {
    Route::get('dashboard', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('dashboard');
    Route::get('comments', [App\Http\Controllers\Admin\CommentsController::class, 'index'])->name('comments.index');
    Route::get('comments/{comment}', [App\Http\Controllers\Admin\CommentsController::class, 'show'])->name('comments.show');
    Route::patch('comments/{comment}', [App\Http\Controllers\Admin\CommentsController::class, 'approve'])->name('comments.approve');
    Route::delete('comments/{comment}', [App\Http\Controllers\Admin\CommentsController::class, 'destroy'])->name('comments.delete');
    Route::get('users', [App\Http\Controllers\Admin\UsersController::class, 'index'])->name('users.index');
    Route::get('users/{user}', [App\Http\Controllers\Admin\UsersController::class, 'edit'])->name('users.edit');
    Route::patch('users/{user}', [App\Http\Controllers\Admin\UsersController::class, 'update'])->name('users.update');
});

Route::resource('articles', \App\Http\Controllers\ArticlesController::class)->middleware(['auth']);

Route::resource('articles.comments', \App\Http\Controllers\CommentsController::class)->only(['store', 'destroy']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
