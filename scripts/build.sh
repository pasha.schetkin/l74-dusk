#!/usr/bin/env bash

set -e

info() {
echo -e "\e[33m[Info]     \e[33m$1 \e[39m $(for i in {12..21} ; do echo -en "\e[33;1;${i}m>\e[0m" ; done ; echo)"
}

success() {
echo -e "\e[32m[Success] \e[32m $1 \e[39m $(for i in {12..21} ; do echo -en "\e[32;1;${i}m>\e[0m" ; done ; echo)"
}

info "./build.sh starts here..."
info "build started under user $(whoami)"
info "build frontend for test...   "
npm install && npm run build

info "Setup environment for dusk test"
cp .env.dusk.local .env


info "Install requirements   "
composer install --no-interaction --prefer-dist

info "Install chrome-driver version  "
php artisan dusk:chrome-driver
info "Run server for dusk test in background mode   "
nohup bash -c "php artisan serve --env=dusk --port=4343 2>&1 &" > serve.out && sleep 5
nohup bash -c "./vendor/laravel/dusk/bin/chromedriver-linux 2>&1 &" > crome-driver.out&& sleep 5
info "Start dusk tests  "
php artisan dusk

info "Removed environment"
rm .env
success "Completed testing!"
