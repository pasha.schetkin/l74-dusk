@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <h2>Users</h2>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Roles</th>
                    <th colspan="2">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>
                            {{$user->name}}
                        </td>
                        <td>
                            <table class="table-bordered">
                                <tbody>
                                @foreach($user->roles as $role)
                                    <tr>
                                        <td>{{$role->name}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                        <td colspan="2">
                            <a role="button" class="btn btn-sm btn-outline-primary" href="{{route('admin.users.edit', compact('user'))}}">
                                Set roles
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>

@endsection
