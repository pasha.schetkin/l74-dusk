$(document).ready(function () {
    $('.close-create-comment-modal').click(() => {
        $("#create-comment").trigger('reset');
    });

    $('#create-comment-btn').click(function (event) {
        event.preventDefault();
        const data = $('#create-comment').serialize();
        const articleId = $('#article_id').val();
        $.ajax({
            url: `/articles/${articleId}/comments`,
            method: "POST",
            data: data
        }).done(function(response) {
            renderData(response.comment);
        }).fail(function(response) {
            console.log(response);
        });
    });

    function renderData(comment) {
        let commentsBlock = $('.scrollit');
        $(commentsBlock).prepend(comment);
        clearForm();
    }

    function clearForm() {
        $("#create-comment").trigger('reset');
        $("#exampleModal").toggle();
        $('.modal-backdrop').remove();
        $('body').removeAttr('class');
        $('body').removeAttr('style');
    }

    $(document).on('click', '.delete-comment', function(e) {
        console.log('Work');
        deleteComment(e);
    });

    const deleteComment = event => {
        let element = event.currentTarget;
        const commentId = $(element).data('comment-id');
        const articleId = $('#article_id').val();
        // url = /articles/{article}/comments/{comment}
        $.ajax({
            method: 'DELETE',
            url: `/articles/${articleId}/comments/${commentId}/`,
            data: {_token: $("meta[name='csrf-token']").attr('content')}
        }).done(response => {
            console.log('response => ', response);
            $(`#comment-${commentId}`).remove();
        }).fail(errorResponse => {
            console.log('error response => ', errorResponse);
        });
    };

});
