$(document).ready(function () {
    $('.approve-comment').click(function(e) {
        const element = e.currentTarget;
        const commentId = $(element).data('comment-id');
        const token = $(element).data('csrf-token');

        $.ajax({
            method: 'patch',
            url: `/admin/comments/${commentId}`,
            data: {'_token': token}

        }).done(function(response) {
            console.log(response);
            $(`#comment-body-${commentId}`).remove();
        }).fail(responseFail => {
            console.log(responseFail);
        })
    })
})
