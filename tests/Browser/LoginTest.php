<?php

namespace Tests\Browser;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group login
     * @throws \Throwable
     */
    public function testSuccessLogin(): void
    {
        $email = $this->faker->email();
        User::factory()->state([
            'email' => $email
        ])->create();

        $this->browse(function (Browser $browser) use ($email) {
            $browser->visit('/login')
                    ->assertSee('Login')
                    ->typeSlowly('email', $email)
                    ->typeSlowly('password', 'password')
                    ->press('Login')
                    ->assertPathIs('/');
        });
    }

    /**
     * A Dusk test example.
     * @group login
     * @throws \Throwable
     */
    public function testUserTryingLoginWithNonExistEmail()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->assertSee('Login')
                ->typeSlowly('email', 'non_exist@gmail.email')
                ->typeSlowly('password', 'password')
                ->press('Login')
                ->assertPathIs('/login')
                ->assertSee('These credentials do not match our records.');
        });
    }

    /**
     * A Dusk test example.
     * @group login1
     * @throws \Throwable
     */
    public function testAuthUser()
    {
        $user = User::factory()->state([
            'email' => $this->faker->email()
        ])->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)->visit(route('articles.show', ['article' => 1]));
        });
    }
}
