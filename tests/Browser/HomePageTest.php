<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\HomePage;
use Tests\DuskTestCase;

class HomePageTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     * @group home
     */
    public function testUserGetRootPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new HomePage());
        });
    }
}
