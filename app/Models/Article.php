<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 * @property string title
 * @property string content
 * @property int user_id
 * @property User user
 * @property Comment[]|Collection comments
 */
class Article extends AbstractModel
{
    /**
     * @var string[]
     */
    protected $fillable = ['title', 'content', 'user_id'];

    /**
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class)->latest();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
    public function moderationComments(): mixed
    {
        return $this->comments->where('moderation', 1);
    }
}
