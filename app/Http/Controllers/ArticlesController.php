<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $articles = Article::paginate(env('COUNT_ARTICLES_ON_PAGE', 8));
        return view('articles.index', compact('articles'));
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return Application|Factory|View|Response
     */
    public function show(Article $article)
    {
        $article->setRelation('comments', $article->comments()->paginate(2));
        return view('articles.show', compact('article'));
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Article::class);
        return view('articles.create');
    }

    /**
     * @param ArticleRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(ArticleRequest $request): RedirectResponse
    {
        $this->authorize('create', Article::class);
        $article = new Article();
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $article->user_id = $request->user()->id;

        // Либо
        //$article->user()->associate($request->user());
        $article->save();
        return redirect()->route('articles.index');
    }


    /**
     * @param Article $article
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function edit(Article $article)
    {
        $this->authorize('edit', $article);
        return view('articles.edit', compact('article'));
    }

    /**
     * Update the given blog article.
     *
     * @param ArticleRequest $request
     * @param Article $article
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(ArticleRequest $request, Article $article): RedirectResponse
    {
        $this->authorize('update', $article);
        $article->update($request->all());
        return redirect()->route('articles.index');
    }

    /**
     * @param Article $article
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Article $article): RedirectResponse
    {
        $this->authorize('delete', $article);
        $article->delete();
        return redirect()->route('articles.index');
    }
}
