# Jenkins server
## Установка пхп
	1. sudo apt-get update
	2. sudo apt-get install php8.1 php8.1-curl php8.1-mbstring php-sqlite3 php-xml php8.1-zip php-mbstring php-xml php8.1-fpm

## Установка стабильных зависимостей для фронтенда
	1. sudo apt install nodejs
	2. sudo apt install npm
	3. sudo npm install -g n
	4. sudo n stable

## Установка стабильного хрома для запусков тестов
	1. wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	2. sudo apt install ./google-chrome-stable_current_amd64.deb

## Установка composer
    1. sudo apt-get install php-cli unzip
    2. curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
    3. sudo php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer
    4. composer
